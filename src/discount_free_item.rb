require_relative 'discount'
require_relative 'purchased_item'

class DiscountFreeItem < Discount
  attr_reader :free_item

  def initialize(sku, free_item, with_sku)
    @free_item = free_item
    @with_sku = with_sku
    super sku
  end

  def get_discounts(cart, new_item)
    discounts = []
    return discounts unless new_item.sku == @sku

    sku_items = cart.select{ |i| i.sku == @sku }.to_a
    with_sku_items = cart.select{ |i| i.sku == @with_sku }.to_a

    if with_sku_items.length > sku_items.length
      discounts << PurchasedItem.new(nil, "Free #{@free_item.name} when bundled with #{@with_sku}", - @free_item.price, true)
    end
    discounts
  end
end
