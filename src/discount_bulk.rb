require_relative 'discount'
require_relative 'purchased_item'

class DiscountBulk < Discount

  def initialize(sku, from_quantity, discounted_price)
    @from_quantity = from_quantity
    @discounted_price = discounted_price
    super sku
  end

  def get_discounts(cart, new_item)
    discounts = []
    return discounts unless new_item.sku == @sku

    sku_items = cart.select{ |i| i.sku == @sku }.to_a

    if sku_items.length == @from_quantity - 1
      #when this is the first time the discount is applied, apply it for all the other items.
      @from_quantity.times do
        discounts << PurchasedItem.new(nil, "Bulk discount for: #{@sku}", @discounted_price - sku_items[0].price , true)
      end
    elsif sku_items.length >= @from_quantity
      discounts << PurchasedItem.new(nil, "Bulk discount for: #{@sku}", @discounted_price - sku_items[0].price , true)
    end

    discounts
  end
end
