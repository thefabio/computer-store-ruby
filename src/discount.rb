class Discount
  attr_reader :sku

  def initialize(sku)
    @sku = sku
  end

  def get_discounts(cart, new_item)
    []
  end
end
