class PurchasedItem
  attr_reader :sku
  attr_reader :name
  attr_reader :price
  attr_reader :is_discount

  def initialize(sku, name, price, is_discount = false)
    @sku = sku
    @name = name
    @price = price
    @is_discount = is_discount
  end
end
