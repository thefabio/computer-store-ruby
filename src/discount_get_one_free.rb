require_relative 'discount'
require_relative 'purchased_item'

class DiscountGetOneFree < Discount

  def initialize(sku, quantity)
    @quantity = quantity
    super sku
  end

  def get_discounts(cart, new_item)
    discounts = []
    return discounts unless new_item.sku == @sku

    sku_items = cart.select{ |i| i.sku == @sku }.to_a

    if sku_items.length > 0 && (sku_items.length + 1) % @quantity == 0
      discounts << PurchasedItem.new(nil, "Deal, a free: #{@sku} for each #{@quantity}", - sku_items[0].price , true)
    end

    discounts
  end
end
