require_relative 'purchased_item'
require_relative 'discount_free_item'
require_relative 'discount_get_one_free'
require_relative 'discount_bulk'

class Checkout
  attr_reader :shopping_cart

  def load_items
    #TODO: possible improvement --> should read this data from a file
    @available_items = []

    @available_items << PurchasedItem.new('ipd', 'Super iPad', 549.99)
    @available_items << PurchasedItem.new('mbp', 'MacBook Pro', 1399.99)
    @available_items << PurchasedItem.new('atv', 'Apple TV', 109.50)
    @available_items << PurchasedItem.new('vga', 'VGA adapter', 30)
  end

  def initialize(pricing_rules)
    @pricing_rules = pricing_rules

    #purchased items and discounts will be placed here
    @shopping_cart = []

    load_items
  end

  def scan(sku)
    new_purchased_item = @available_items.select{|i| i.sku == sku}.first

    if new_purchased_item.nil?
      puts "The sku '#{sku}' is invalid"
      return
    end

    discounts = []
    @pricing_rules.each do |r|
      r.get_discounts(@shopping_cart, new_purchased_item).each do |d|
        discounts << d
      end
    end

    @shopping_cart << new_purchased_item
    discounts.each do |d|
      @shopping_cart << d
    end
  end

  def total
    total_sum = 0
    @shopping_cart.each do |item|
      total_sum += item.price
    end

    total_sum
  end
end
