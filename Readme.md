Response to the Conputer Store code challenge
=======

This is my response to the code challenge prosed here: [Computer Store](ComputerStore.md)

I chose to use the Ruby Language with the test framework RSpec (v3.6)

This response was created with Ruby version 2.4.1

To install RSpec and its dependencies please run the command:
```
bundle install
```

The test specs can be run with the command
```
rspec /spec
```

The response application can be run with the command:
```
ruby process_sale.rb
```
