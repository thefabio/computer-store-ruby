require 'spec_helper'

RSpec.describe Discount do

  it 'applies bulk discount when number of items is equal to the discount rule' do
    price_rule = DiscountBulk.new('abc', 3, 30)

    cart = []

    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('qwe','Item 2', 300)
    cart << PurchasedItem.new('qwe','Item 2', 300)

    discounts = price_rule.get_discounts(cart, PurchasedItem.new('abc','Item 1', 100))

    expect(discounts.length).to eq(3)
    expect(discounts[0].price).to eq(-70)
  end

  it 'does not apply bulk discount when number of items is less then the discount rule' do
    price_rule = DiscountBulk.new('abc', 4, 30)

    cart = []

    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('qwe','Item 2', 300)
    cart << PurchasedItem.new('qwe','Item 2', 300)

    discounts = price_rule.get_discounts(cart, PurchasedItem.new('abc','Item 1', 100))

    expect(discounts.length).to eq(0)
  end

  it 'applies Get-One-free discount when there are bundles of the item in the cart' do
    price_rule = DiscountGetOneFree.new('abc', 4)

    cart = []

    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('qwe','Item 2', 300)
    cart << PurchasedItem.new('qwe','Item 2', 300)

    discounts = price_rule.get_discounts(cart, PurchasedItem.new('abc','Item 1', 100))

    expect(discounts.length).to eq(1)
    expect(discounts[0].price).to eq(-100)
  end

  it 'does not apply Get-One-free discount when there not enough are bundles of the item in the cart' do
    price_rule = DiscountGetOneFree.new('abc', 4)

    cart = []

    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('abc','Item 1', 100)
    cart << PurchasedItem.new('qwe','Item 2', 300)
    cart << PurchasedItem.new('qwe','Item 2', 300)

    discounts = price_rule.get_discounts(cart, PurchasedItem.new('abc','Item 1', 100))

    expect(discounts.length).to eq(0)
  end

  it 'applies Free-Item discount when the reference item is in the cart' do
    item = PurchasedItem.new('abc', 'Item', 20)
    price_rule = DiscountFreeItem.new(item.sku, item, 'ref')

    cart = []

    cart << PurchasedItem.new('ref','Item 1', 100)
    cart << PurchasedItem.new('c22','Item 2', 120)
    cart << PurchasedItem.new('a33','Item 3', 130)
    cart << PurchasedItem.new('qwe','Item 4', 300)
    cart << PurchasedItem.new('qwe','Item 4', 300)

    discounts = price_rule.get_discounts(cart, item)

    expect(discounts.length).to eq(1)
    expect(discounts[0].price).to eq(-20)
  end

  it 'does not apply Free-Item discount when the reference item is not in the cart' do
    item = PurchasedItem.new('abc', 'Item', 20)
    price_rule = DiscountFreeItem.new(item.sku, item, 'ref')

    cart = []

    cart << PurchasedItem.new('c22','Item 2', 120)
    cart << PurchasedItem.new('a33','Item 3', 130)
    cart << PurchasedItem.new('qwe','Item 4', 300)
    cart << PurchasedItem.new('qwe','Item 4', 300)

    discounts = price_rule.get_discounts(cart, item)

    expect(discounts.length).to eq(0)
  end
end
