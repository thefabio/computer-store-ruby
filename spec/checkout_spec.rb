require 'spec_helper'

RSpec.describe Checkout do

  before(:each) do
    pricing_rules = []
    pricing_rules << DiscountGetOneFree.new('atv', 3)
    pricing_rules << DiscountBulk.new('ipd', 4, 499.99)
    pricing_rules << DiscountFreeItem.new('vga', PurchasedItem.new('vga', 'VGA adapter', 30), 'mbp')

    @co = Checkout.new pricing_rules
  end

  it 'Should output total: $249.00 for Example 1' do
    @co.scan('atv')
    @co.scan('atv')
    @co.scan('atv')
    @co.scan('vga')

    expect(@co.total).to eq(249)
  end

  it 'Should output total: $2718.95 for Example 2' do
    @co.scan('atv')
    @co.scan('ipd')
    @co.scan('ipd')
    @co.scan('atv')
    @co.scan('ipd')
    @co.scan('ipd')
    @co.scan('ipd')

    expect(@co.total).to eq(2718.95)
  end

  it 'Should output total: $1949.98 for Example 3' do
    @co.scan('mbp')
    @co.scan('vga')
    @co.scan('ipd')

    expect(@co.total).to eq(1949.98)
  end
end
