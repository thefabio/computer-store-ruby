require_relative 'src/checkout'

pricing_rules = []
pricing_rules << DiscountGetOneFree.new('atv', 3)
pricing_rules << DiscountBulk.new('ipd', 4, 499.99)
pricing_rules << DiscountFreeItem.new('vga', PurchasedItem.new('vga', 'VGA adapter', 30), 'mbp')

co = Checkout.new(pricing_rules)

puts 'DiUS Computer Store'
puts 'Please enter one of the sku values:'
puts '   ipd'
puts '   mbp'
puts '   atv'
puts '   vga'
puts 'Please input the following command when finished:'
puts '   END'

loop do
  read_prompt = STDIN.gets
  read_prompt.strip!

  if read_prompt == 'END'
    puts 'Checked Out items:'
    co.shopping_cart.each do |i|
      if i.is_discount
        puts "#{i.name} -- $ #{i.price}"
      else
        puts "#{i.name} (#{i.sku}) -- $ #{i.price}"
      end
    end
    puts "*** Total: $ #{co.total} ***"
    break
  end

  valid_sku = %w(ipd mbp atv vga)

  if valid_sku.include?(read_prompt)
    co.scan(read_prompt)
  else
    puts "invalid sku '#{read_prompt}'"
  end
end
